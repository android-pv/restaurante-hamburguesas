package com.example.menu_restaurante;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	String platos[]={"Fricase", "Sajta", "Timpu", "Plato pace�o","Chairo"};
	int precios[]={26,32,38,25,22};
	int imagenes[] = {R.drawable.fricase, R.drawable.sajta,R.drawable.timpu,R.drawable.platopacenio,R.drawable.chairo,};
	int pedido[]=new int[imagenes.length];
	int i=0;
	
	public void cambioDePlato(){
		ImageView img = (ImageView) findViewById(R.id.imageView1);
		TextView titulo= (TextView)findViewById(R.id.textView1);
		titulo.setText(platos[i] + " " + precios[i]+ " Bs");
		img.setImageResource(imagenes[i]);
	}
	public void siguiente(View vista){
		i++;
		if( i >= imagenes.length)
			i=0;
		cambioDePlato();
		EditText c = (EditText)findViewById(R.id.editText1);
		c.setText(pedido[i]+"");
	}
	public void anterior(View vista){
		i--;
		if( i < 0)
			i=imagenes.length-1;
		cambioDePlato();
		EditText c = (EditText)findViewById(R.id.editText1);
		c.setText(pedido[i]+"");
	}
	public void comprar(View vista){
		pedido[i]++;
		EditText c = (EditText)findViewById(R.id.editText1);
		c.setText(pedido[i]+"");
		EditText t = (EditText)findViewById(R.id.editText2);
		t.setText(calculaTotal()+"");
	}
	public void devolver(View vista){
		if(pedido[i]>0)
			pedido[i]--;
		EditText c = (EditText)findViewById(R.id.editText1);
		c.setText(pedido[i]+"");
		EditText t = (EditText)findViewById(R.id.editText2);
		t.setText(calculaTotal()+"");
	}
	public int calculaTotal(){
		int s=0;;
		for(int j=0;j<pedido.length;j++)
			s=s+pedido[j]*precios[j];
		return s;
	}
	public void c(){
		for(int p=0;p<pedido.length;p++){
			pedido[p]=0;
		}
		EditText c = (EditText)findViewById(R.id.editText1);
		c.setText("");
		EditText t = (EditText)findViewById(R.id.editText2);
		t.setText("");
	}
	public void f(View vista){
		String mensaje = "";
		for(int j=0; j<pedido.length;j++){
			mensaje+=platos[j]+ " "+precios[j]+" Bs, "+pedido[j]+" ->   "+precios[j]*pedido[j]+"\n";
		}
		mensaje+="Total: "+calculaTotal();
	
		Toast toast1 =Toast.makeText(getApplicationContext(),mensaje, Toast.LENGTH_LONG);
	        toast1.show();
	    c();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
